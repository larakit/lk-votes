<?php

namespace Larakit\Votes;

use App\Me\Me;
use Larakit\Controller;
use Larakit\TelegramBot;

class VotesController extends Controller {
    
    function vote($vote_value) {
        $diff        = Me::voteSize();
        $vote_value  = ($vote_value > 0) ? $diff : (0 - $diff);
        $object_type = \Request::route('object_type');
        $object_id   = (int) \Request::route('object_id');
        if(!\Auth::getUser()) {
            throw new \Exception('Вы не авторизованы!');
        }
        $model_name = \Larakit\Models\LarakitVote::getObjectType($object_type);
        if(!$model_name) {
            throw new \Exception('Не известный науке тип!');
        }
        if(!class_exists($model_name)) {
            throw new \Exception('Не известный науке тип!');
        }
        $obj = $model_name::find($object_id);
        if(!$obj) {
            throw new \Exception('Оцениваемый объект отсутствует!');
        }
        TelegramBot::add($obj);
        TelegramBot::add(Me::hashtag().' оценил в '.$vote_value);
        TelegramBot::send('notify');
        
        $vote       = \Larakit\Models\LarakitVote::firstOrCreate([
            'author_id'     => \Auth::getUser()->id,
            'voteable_id'   => $obj->id,
            'voteable_type' => $obj->getMorphClass(),
        ]);
        $vote->vote = $vote_value;
        $vote->save();
        
        return [
            'result'  => 'success',
            'votes'   => \Larakit\Models\LarakitVote::where('voteable_id', $obj->id)
                ->where('voteable_type', $obj->getMorphClass())
                ->get(),
            'message' => 'Ваш голос принят',
        ];
    }
    
    function up() {
        return $this->vote(1);
    }
    
    function down() {
        return $this->vote(-1);
    }
}