<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLarakitVotesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('larakit__votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vote');
            $table->integer('author_id');
            $table->integer('voteable_id');
            $table->string('voteable_type');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('larakit__votes');
    }
}
