<?php
\Larakit\Route\Route::ajax('votes')
    ->addMiddleware(['web', 'auth'])
    ->setNamespace('Larakit\Votes')
    ->addSegment('{object_type}')
    ->addSegment('{object_id}')
    ->addSegment('up')
    ->setAction('up')
    ->put('post')
    ->popSegment()
    ->addSegment('down')
    ->setAction('down')
    ->put('post');

//    ->setUses(function ($object_type, $object_id) {
//        $model_name = \Larakit\Models\LarakitVote::getObjectType($object_type);
//        if(!$model_name) {
//            throw new Exception('Не известный науке тип!');
//        }
//        if(!class_exists($model_name)) {
//            throw new Exception('Не известный науке тип!');
//        }
//        $o = $model_name::find($object_id);
//        if(!$o) {
//            throw new Exception('Оцениваемый объект отсутствует!');
//        }
//        $ret    = [
//            'objects' => [],
//        ];
//        $models = \Larakit\Models\LarakitVote::where('voteable_id', '=', $o->id)
//            ->where('voteable_type', '=', $o->getMorphClass())
//            ->get();
//        foreach($models as $model) {
//            $ret['models'][]
//        }
//
//        return $models;
//    })
//    ->put('get')
//    ->setUses(function ($object_type, $object_id) {
//        $value  = (bool) Request::get('value');
//        $usr_id = 0;
//        if(\Auth::getUser()) {
//            $usr_id = \Auth::getUser()->id;
//        }
//        $model_name = \Larakit\Models\LarakitVote::getObjectType($object_type);
//        if(!$model_name) {
//            throw new Exception('Не известный науке тип!');
//        }
//        if(!class_exists($model_name)) {
//            throw new Exception('Не известный науке тип!');
//        }
//        $o = $model_name::find($object_id);
//        if(!$o) {
//            throw new Exception('Оцениваемый объект отсутствует!');
//        }
//
//        \Larakit\Models\LarakitVote::voteSet($o, 1);
//
//        return [
//            'result'  => 'success',
//            'message' => 'Ваш голос принят',
//        ];
//
//    })
//    ->put('post');