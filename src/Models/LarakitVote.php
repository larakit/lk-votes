<?php
/**
 * Created by PhpStorm.
 * User: koksharov
 * Date: 30.11.16
 * Time: 14:08
 */

namespace Larakit\Models;

use App\Me\Me;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class LarakitComment
 * @package Larakit\Models
 * @mixin \Eloquent
 */
class LarakitVote extends Model {

    protected $table    = 'larakit__votes';
    protected $hidden   = [
        'voteable_id',
        'voteable_type',
    ];
    protected $appends  = [
        'is_my',
        'author_name',
    ];
    protected $fillable = [
        'voteable_id',
        'voteable_type',
        'author_id',
        'vote',
    ];

    public function voteable() {
        return $this->morphTo();
    }

    static protected $object_types = [];

    function getIsMyAttribute() {
        return $this->author_id == Me::id();
    }
    function getAuthorNameAttribute() {
        return $this->author->name .' ['. $this->author->city.']';
    }

    static function registerObjectType($model_class, $key = null) {
        self::$object_types[$model_class] = ($key ? $key : md5($model_class));
    }

    function author() {
        return $this->belongsTo(User::class, 'author_id');
    }

    static function getObjectType($key) {
        return array_search($key, self::$object_types);
    }

    static function getObjectCode($model_name) {
        return Arr::get(self::$object_types, $model_name);
    }
}

LarakitVote::creating(function ($model) {
    if(!$model->voteable_type) {
        $model->voteable_type = '';
    }
    if(!$model->voteable_id) {
        $model->voteable_id = 0;
    }
    if(!$model->author_id) {
        $model->author_id = Me::id();
    }
});